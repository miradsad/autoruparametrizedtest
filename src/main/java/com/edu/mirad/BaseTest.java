package com.edu.mirad;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
@Slf4j
abstract public class BaseTest {
    protected final String MY_URL = "https://auto.ru/";
    public void setUp(){
        log.info("Инициализация веб-дайвера...");
        ChromeOptions options = new ChromeOptions();
        ChromeDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.timeout = 2000;
        Configuration.headless = false;

    }
    @Before
    public void init(){
        setUp();
    }
    @AfterEach
    public void tearDown(){
        log.info("Закрываем веб-драйвер...");
        Selenide.closeWebDriver();
    }
}