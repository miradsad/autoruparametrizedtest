package com.edu.mirad;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

public class CarValuationTest extends BaseTest {
    @ParameterizedTest
    @EnumSource(EmptyFieldsEnum.class)
    public void checkValuationFields(EmptyFieldsEnum emptyFieldsEnum){
        MainPage mainPage = new MainPage(MY_URL);
        mainPage.hoverNavigation().navigateIntoValuation();
        ValuationPage valuationPage = new ValuationPage();
        valuationPage.enterVin(emptyFieldsEnum.getVin()).enterMilerange(emptyFieldsEnum.getMilerange()).enterCity(emptyFieldsEnum.getCity()).doValuation();
        switch (emptyFieldsEnum){
            case EMPTY_ALL ->
                    Assertions.assertAll(
                    ()-> Assertions.assertEquals("Введите госномер или ВИН",valuationPage.getVinError()),
                    ()-> Assertions.assertEquals("Введите пробег", valuationPage.getMilerangeError()),
                    ()-> Assertions.assertEquals("Введите город",valuationPage.getCityError())
                );
            case EMPTY_VIN -> Assertions.assertAll(
                    ()-> Assertions.assertEquals(null,valuationPage.getVinError()),
                    ()-> Assertions.assertEquals("Введите пробег", valuationPage.getMilerangeError()),
                    ()-> Assertions.assertEquals("Введите город",valuationPage.getCityError())
            );
            case EMPTY_CITY -> Assertions.assertAll(
                    ()-> Assertions.assertEquals("Введите госномер или ВИН",valuationPage.getVinError()),
                    ()-> Assertions.assertEquals("Введите пробег", valuationPage.getMilerangeError()),
                    ()-> Assertions.assertEquals(null,valuationPage.getCityError())
            );
            case EMPTY_MILERANGE -> Assertions.assertAll(
                    ()-> Assertions.assertEquals("Введите госномер или ВИН",valuationPage.getVinError()),
                    ()-> Assertions.assertEquals(null, valuationPage.getMilerangeError()),
                    ()-> Assertions.assertEquals("Введите город",valuationPage.getCityError())
            );
        }
    }
}
