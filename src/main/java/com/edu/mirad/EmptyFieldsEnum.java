package com.edu.mirad;

public enum EmptyFieldsEnum {
    EMPTY_VIN(null, "15000", "Санкт-Петербург"),
    EMPTY_MILERANGE("Z94CB41AACR123456", null, "Санкт-Петербург"),
    EMPTY_CITY("Z94CB41AACR123456", "15000", null),
    EMPTY_ALL(null, null, null);

    private final String vin;
    private final String milerange;
    private final String city;
    EmptyFieldsEnum(String vin, String milerange, String city){
        this.vin = vin;
        this.milerange = milerange;
        this.city = city;
    }

    public String getVin(){
        return vin;
    }
    public String getMilerange(){
        return milerange;
    }
    public String getCity(){
        return city;
    }
}
