package com.edu.mirad;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class MainPage {
    private final SelenideElement navigationElement = $x(".//*[@class=\"Dropdown__switcher HeaderBurger__switcher\"]");
    private final SelenideElement valuationButton = $x(".//*[text()=\"Оценка авто\" and @data-from=\"buter_autoru_evaluation\"]");
    MainPage(String url){
        open(url);
    }
    public MainPage hoverNavigation(){
        System.out.println("Hover navigation...");
        navigationElement.shouldBe(visible).hover();
        System.out.println("Hovering navigation");
        return this;
    }
    public MainPage navigateIntoValuation(){
        System.out.println("Going to valuation page");
        valuationButton.shouldBe(visible).click();
        System.out.println("Valuation opened");
        return this;
    }
}
