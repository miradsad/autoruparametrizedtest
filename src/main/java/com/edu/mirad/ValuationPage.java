package com.edu.mirad;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.NoSuchElementException;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ValuationPage {
    private final SelenideElement vinInput = $x(".//*[contains(text(),'Госномер или VIN')]/following-sibling::div/input");
    private final SelenideElement mileRangeInput = $x(".//*[contains(text(),'Пробег, км')]/following-sibling::div/input");
    private final SelenideElement cityInput = $x(".//*[contains(text(),'Город продажи')]/following-sibling::div/input");
    private final SelenideElement submitButton = $x(".//*[contains(@class, \"submitButton\")]");
    private final SelenideElement vinError = $x(".//*[contains(text(),'Введите госномер или ВИН')]");
    private final SelenideElement milrangeError = $x(".//*[contains(text(),'Введите пробег')]");
    private final SelenideElement cityError = $x(".//*[contains(text(),'Введите город')]");

    public ValuationPage enterVin(String vin){
        vinInput.shouldBe(visible).setValue(vin);
        return this;
    }
    public ValuationPage enterMilerange(String milerange){
        mileRangeInput.shouldBe(visible).setValue(milerange);
        return this;
    }
    public ValuationPage enterCity(String city){
        cityInput.shouldBe(visible).setValue(city);
        return this;
    }
    public ValuationPage doValuation(){
        submitButton.shouldBe(visible).click();
        return this;
    }
    public String getVinError(){
        try {
            System.out.println(vinError.getText());
            return vinError.getText();
        } catch (NoSuchElementException e) {
            System.out.println("Элемент не был найден");
            return null;
        }
    }
    public String getMilerangeError(){
        try {
            System.out.println(milrangeError.getText());
            return milrangeError.getText();
        } catch (NoSuchElementException e) {
            System.out.println("Элемент не был найден");
            return null;
        }
    }
    public String getCityError(){
        try {
            System.out.println(cityError.getText());
            return cityError.getText();
        } catch (NoSuchElementException e) {
            System.out.println("Элемент не был найден");
            return null;
        }
    }
}
